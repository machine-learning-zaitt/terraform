provider "aws" {
access_key = var.access_key
secret_key = var.secret_key
region = var.region
}

resource "aws_instance" "airflow-dev" {
    ami = var.instance_img
    instance_type = var.instance_type
    key_name = "eduardo-teste"
    security_groups = ["airflow"]
    tags = {
        Name = "airflow"    
    }
}

resource "aws_instance" "airflow-homo" {
    ami = var.instance_img
    instance_type = var.instance_type
    key_name = "eduardo-teste"
    security_groups = ["airflow"]
    tags = {
        Name = "airflow-homo"    
    }
}


