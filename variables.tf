variable "access_key" {
description = "The AWS access key."
}

variable "secret_key" {
description = "The AWS secret key."
}

variable "region" {
type = string
description = "The AWS region."
default = "sa-east-1"
}

variable "instance_img" {
type = string
description = "The instance img."
default = "ami-02c8813f1ea04d4ab"
}

variable "instance_type" {
type = string
description = "The instance_type."
default = "t2.medium"
}
