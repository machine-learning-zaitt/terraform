# Installing Terraform:

## Linux:
`cd /tmp`

`wget https://releases.hashicorp.com/terraform/0.12.18/terraform_0.12.18_linux_amd64.zip`

`unzip https://releases.hashicorp.com/terraform/0.12.18/terraform_0.12.18_linux_amd64.zip`

`sudo mv terraform /usr/local/bin/`

`sudo chown -R root:root /usr/local/bin/terraform`

`terraform version`

## Windows:
Be a real programmer and use Linux

## MacOS:
Seriously? Use Linux



# Running Project

1- Git clone

2- `terraform init` on the project's folder

3- Configure credentials (optional)

4- `terraform plan`


## Configuring Credentials
1. Loading variables from command line flags.
 terraform plan -var 'access_key=abc123' -var 'secret_key=
abc123'
2. Loading variables from a file.
 terraform plan -var-file base.tfvars
3. Loading variables from environment variables.
 TF_VAR_access_code=abc123
